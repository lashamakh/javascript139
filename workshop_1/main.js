// console.log("Hello world");
// firstName = "John";
// firstname = "Gio";
// FIRSTNAME = 'Nana';

// assignment operator (მინიჭების ოპერატორი)
let firstName = "Jane";
// let first_name = "Jane";  // snake case
// var lastName = "Doe"; // outdated
const lastName = "Doe";

firstName = "John";
// lastName = "C"; // Error
// concatenation 
console.log(firstName + ' ' + lastName);
console.log('hello' + ' ' + firstName);
{
    // block-scope, local scope
    let username = "Legend27";
    // console.log(firstName);
    console.log(username);
}

console.log((10 + 20) / 100) // 0.3
