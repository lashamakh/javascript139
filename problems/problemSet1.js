/*
Problem 1:

შექმენით შემდეგი ცვლადები და მიანიჭეთ მათ თქვენთვის სასურველი მნიშვნელობები:
name
lastName
birthYear
gender
*/

/*
Problem 2:

How many expressions are in the following statement?
*/

100 - ((10 / 2) * 7) / 5

/*
Problem 3:

შექმენით 2 ცვალდი (country, population).
country ცვლადს მიანიჭეთ ნებისმიერი ქვეყნის სახელი, დაბალ რეგისტრში (lower case)
population ცვლადს მიანითჭეთ ქვეყნის შესაბამისი მაცხოვრებლების რაოდენობა მილიონებში (მაგ. 3.5 და არა 3500000)
გამოიტანეთ ეს მიშნველობები შემდეგი სახით
„United Kingdom’s population is 67.33 million“

*/
