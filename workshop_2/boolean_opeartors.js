let isRaining = false;
let isWindy = true;
let isSunny = true;
// console.log(is_raining);

// and -> &&
console.log(isRaining && isWindy);
// or -> ||
console.log(isRaining || isWindy);

// not -> !
console.log(!isRaining && !isWindy);
console.log(!isRaining);

// expression
console.log(isRaining && isWindy && isSunny && !isRaining);