/* conditionals / control flow */
let age = 31;
let firstName = 'Vanya';

if (age < 18) {
    // block / code fragment
    console.log('Underage, you cannot enter this site!');
} else if (age > 30) {
    console.log('You are more than welcome!');
} else if (firstName.toLowerCase() === 'putin' || firstName.toLocaleLowerCase() === 'vanya') {
    console.log('You are not allowed!');
} else {
    console.log('You may enter!');
}
